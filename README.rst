=============
Mailman Web
=============

This is a Django project that contains default settings and url settings for
Mailman 3 Web Interface. It consists of the following sub-projects:

* Postorius
* Hyperkitty


=======
Install
=======

To install this project, you run::

  $ pip3 install mailman-web

If you want to install the latest development version from Git, run::

  $ git clone git@gitlab.com:mailman/mailman-web
  $ cd mailman-web
  $ pip install .


=======
License
=======

Mailman suite is licensed under the
`GPL v3.0 <http://www.gnu.org/licenses/gpl-3.0.html>`_

Copyright (C) 2019 by the Free Software Foundation, Inc.
